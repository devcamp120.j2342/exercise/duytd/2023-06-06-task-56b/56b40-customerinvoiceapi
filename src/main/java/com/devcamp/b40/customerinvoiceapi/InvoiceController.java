package com.devcamp.b40.customerinvoiceapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b40.customerinvoiceapi.models.Customer;
import com.devcamp.b40.customerinvoiceapi.models.Invoice;

@RestController
@RequestMapping("/api")
public class InvoiceController {
    @CrossOrigin
    @GetMapping("/invoices")

    public ArrayList<Invoice> getInvoices(){
        Customer customer1 = new Customer(1, "Đường Công Minh", 10);
        Customer customer2 = new Customer(2, "Đặng Vương Hưng", 20);
        Customer customer3 = new Customer(3, "Trần Thanh Hai", 30);

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Invoice invoice1 = new Invoice(1, customer1, 104500);
        Invoice invoice2 = new Invoice(2, customer2, 180000);
        Invoice invoice3 = new Invoice(3, customer3, 76500);
        
        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);

        ArrayList<Invoice> arrListInvoice = new ArrayList<Invoice>();

        arrListInvoice.add(invoice1);
        arrListInvoice.add(invoice2);
        arrListInvoice.add(invoice3);

        return arrListInvoice;

    }
}
